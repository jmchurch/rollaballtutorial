using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelTwoStart : MonoBehaviour
{
    public GameObject player;

    private PlayerControllerTwo playerControllerScript;

    void Awake()
    {
        // setting the jump allowed to true, this is pretty much just for if you are starting straight from level two
        // this should run at the start of the level
        playerControllerScript = player.GetComponent<PlayerControllerTwo>();
        playerControllerScript.setJumpAllowed(true);
    }
}
