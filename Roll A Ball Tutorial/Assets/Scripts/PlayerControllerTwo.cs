using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerControllerTwo : MonoBehaviour
{
    public float distToGround;
    public float speed = 0;
    public Transform spawnPoint;
    public AudioSource[] levelAudioClipList;
    public Transform endPoint;
    public AudioSource ballImpact;
    public AudioSource clipOne;
    public GameObject transformOneEffect;
    public ParticleSystem ballImpactParticles;

    private Collider rb_Collider;
    private Rigidbody rb;
    private float oldVelocity;
    private float playThreshold = 10.0f;
    private int audioClipNumber = 0;
    private bool transformOne = false;
    private float jumpForce = 9000f;


    private static bool jumpAllowed = false;
    private static int levelNumber = 1;


    private void Awake()
    {
        ballImpactParticles.Pause();
    }
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb_Collider = rb.GetComponent<Collider>();
        distToGround = rb_Collider.bounds.extents.y + (float)0.1;

        oldVelocity = rb.velocity.sqrMagnitude;
        playThreshold *= playThreshold; // so that this float works with sqrMagnitude
    }

    private void Update()
    {
        // when jump button is pressed add an upward force to the player
        if (Input.GetButtonDown("Jump") && isGrounded() && jumpAllowed)
        {
            Debug.Log("Jumped");
            rb.AddForce(new Vector3(0f, jumpForce, 0f));
        }

        if (transformOne && rb.velocity.magnitude > 0)
        {
            rb.velocity = rb.velocity / 1.005f;  
        }
    }

    public void setJumpAllowed(bool a)
    {
        jumpAllowed = a;
    }

    private bool isGrounded()
    {
        return Physics.Raycast(transform.position, -Vector3.up, distToGround);
    }


    void FixedUpdate()
    {
        Vector3 input = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        float cameraRot = Camera.main.transform.rotation.eulerAngles.y;
        // if the ball is grounded then the player will be able to alter the force applied to the ball normally
        if (isGrounded() && !transformOne) rb.AddForce( Quaternion.Euler(0, cameraRot, 0) * input * speed * Time.deltaTime);
        else if (!transformOne) rb.AddForce( Quaternion.Euler(0, cameraRot, 0) * input * speed/4 * Time.deltaTime); // if the ball is not grounded it should be 1/4 as effective movement

        if (oldVelocity - rb.velocity.sqrMagnitude > playThreshold && !transformOne)
        {
            ballImpact.Play(); //  if the impact is greater than the threshold then the impact noise will play
            ParticleSystem particleSystem = Instantiate(ballImpactParticles, rb.transform.position - new Vector3(0f, -.4f, 0f), Quaternion.Euler(0,0,0));
        }
        oldVelocity = rb.velocity.sqrMagnitude;
    }

    void nextLevel()
    {
        levelNumber++;
        Debug.Log("Level " + levelNumber + " load attempted.");
        SceneManager.LoadScene("Level " + levelNumber);
    }



    public IEnumerator spawnParticles(GameObject effect, float i, float amount)
    {
        yield return new WaitForSeconds(amount);
        Instantiate(effect, rb.transform.position + new Vector3(0f, .5f, 0f) + new Vector3(i * 0.99f, 0f, 0f), Quaternion.Euler(0f, 0f, 0f), rb.transform);
        Instantiate(effect, rb.transform.position + new Vector3(0f, -.5f, 0f) + new Vector3(0f, 0f, i * 0.99f), Quaternion.Euler(0f, 0f, 180f), rb.transform);
        Instantiate(effect, rb.transform.position + new Vector3(.5f, 0f, 0f) + new Vector3(0f, 0f, i * 0.99f), Quaternion.Euler(0f, 90f, 0f), rb.transform);
        Instantiate(effect, rb.transform.position + new Vector3(-.5f, 0f, 0f) + new Vector3(0f, 0f, i * 0.99f), Quaternion.Euler(0f, -90f, 0f), rb.transform);
        Instantiate(effect, rb.transform.position + new Vector3(0f, 0f, .5f) + new Vector3(i * 0.99f, 0f, 0f), Quaternion.Euler(90f, 0f, 0f), rb.transform);
        Instantiate(effect, rb.transform.position + new Vector3(0f, 0f, -.5f) + new Vector3(i * 0.99f, 0f, 0f), Quaternion.Euler(-90f, 0f, 0f), rb.transform);
        Instantiate(effect, rb.transform.position + new Vector3(0f, .5f, 0f) + new Vector3(i * -0.99f, 0f, 0f), Quaternion.Euler(0f, 0f, 0f), rb.transform);
        Instantiate(effect, rb.transform.position + new Vector3(0f, -.5f, 0f) + new Vector3(0f, 0f, i * -0.99f), Quaternion.Euler(0f, 0f, 180f), rb.transform);
        Instantiate(effect, rb.transform.position + new Vector3(.5f, 0f, 0f) + new Vector3(0f, 0f, i * -0.99f), Quaternion.Euler(0f, 90f, 0f), rb.transform);
        Instantiate(effect, rb.transform.position + new Vector3(-.5f, 0f, 0f) + new Vector3(0f, 0f, i * -0.99f), Quaternion.Euler(0f, -90f, 0f), rb.transform);
        Instantiate(effect, rb.transform.position + new Vector3(0f, 0f, .5f) + new Vector3(i * -0.99f, 0f, 0f), Quaternion.Euler(90f, 0f, 0f), rb.transform);
        Instantiate(effect, rb.transform.position + new Vector3(0f, 0f, -.5f) + new Vector3(i * -0.99f, 0f, 0f), Quaternion.Euler(-90f, 0f, 0f), rb.transform);
    }

    public IEnumerator waitForNextLevel(float amount)
    {
        yield return new WaitForSeconds(amount);
        nextLevel();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "DeathBox")
        {
            // set the position of the ball to the position of the spawnpoint and reset the velocity to 0
            rb.velocity = new Vector3(0, 0, 0);
            rb.angularVelocity = new Vector3(0, 0, 0);
            this.transform.position = spawnPoint.position;
            Camera.main.GetComponent<CameraController>().forwards();
        }
        else if (other.tag == "RightTurn")
        {
            Camera.main.GetComponent<CameraController>().rightTurn();
        }
        else if (other.tag == "LeftTurn")
        {
            Camera.main.GetComponent<CameraController>().leftTurn();
        }
        else if (other.tag == "Forwards")
        {
            Camera.main.GetComponent<CameraController>().forwards();
        }
        else if (other.tag == "Backwards")
        {
            Camera.main.GetComponent<CameraController>().backwards();
        }
        else if (other.tag == "End")
        {
            rb.velocity = new Vector3(0, 0, 0);
            rb.angularVelocity = new Vector3(0, 0, 0);
            this.transform.position = endPoint.position;
        }
        else if (other.tag == "LevelEnd")
        {
            StartCoroutine(waitForNextLevel(3f));
        }
        else if (other.tag == "Audio")
        {
            Debug.Log("audioClipNumber: " + levelAudioClipList[audioClipNumber]);
            clipOne = levelAudioClipList[audioClipNumber];
            clipOne.Play();
            audioClipNumber++;
        }
        else if (other.tag == "TransformOne")
        {

            rb.constraints = RigidbodyConstraints.FreezePositionY;
            transformOne = true;
            setJumpAllowed(true);
            transformOneEffect.SetActive(true);
            // for loop instantiating effects that should be slightly different around the ball, hopefully looks cool :)
            for (int i = 0; i < 5; i++)
            {
                StartCoroutine(spawnParticles(transformOneEffect, i, .2f));
            }
            StartCoroutine(waitForNextLevel(5f));
        }
    }
}
