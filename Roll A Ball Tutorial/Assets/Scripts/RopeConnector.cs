using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// The main idea of this class right now is trying to get a 
// raycast from the camera to the mouse so that I can access the 
// objects that would eventually be connected
public class RopeConnector : MonoBehaviour
{

    private Rigidbody rb;
    private bool connected;
    private SpringJoint connectedSpring;
    public AudioSource ropeConnectedEffect;
   
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        // checks that the object is within "blank" units, has the tag "Swingable", and the primary mouse button is pushed
        if (Physics.Raycast(mouseRay, out hit, 50) && hit.transform.tag == "Swingable" && Input.GetMouseButtonDown(0))
        {
            // set connected to true so that the 
            connected = true;
            ropeConnectedEffect.Play();

            // make a variable for the object that was hit
            GameObject swingableObject = hit.transform.gameObject;

            // add a spring joint to that object
            swingableObject.AddComponent<SpringJoint>().connectedBody = rb;
            SpringJoint spring = swingableObject.GetComponent<SpringJoint>();
            // setting the anchor of the spring to the local coords of the RayCast hit
            spring.anchor = hit.transform.InverseTransformPoint(hit.point);

            // setting the connecection of the player to the top of the ball
            spring.autoConfigureConnectedAnchor = false;
            spring.connectedAnchor = new Vector3(0f, 0.5f, 0f);

            spring.enablePreprocessing = false;


            // setting the maxDistance of the spring to the current distance between the player and the anchor point
            // dividing by a set variable because I honestly don't know how maxDistance exactly works, this just seems to make the spring the correct length
            spring.maxDistance = 1;

            // Set the Spring
            spring.spring = 38983.9f;

            // Set the Damper
            spring.damper = 31187.12f;

            // make the class variable connectedSpring equal to spring
            this.connectedSpring = spring;

            // VISUALS
            GetComponent<LineRenderer>().enabled = true;
            GetComponent<RopeControllerSimple>().enabled = true;

            // Setting the transforms for RopeControllerSimple
            GetComponent<RopeControllerSimple>().whatTheRopeIsConnectedTo = hit.transform;
            GetComponent<RopeControllerSimple>().whatIsHangingFromTheRope = this.transform;

        }

        // Destroys the SpringJoint if there is one 
        if (Input.GetMouseButtonUp(0) && connected)
        {
            Destroy(connectedSpring);
            GetComponent<RopeControllerSimple>().enabled = false;
            GetComponent<LineRenderer>().enabled = false;
            connected = false;
        }
    }
}
