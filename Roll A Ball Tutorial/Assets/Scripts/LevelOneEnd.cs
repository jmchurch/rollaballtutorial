using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LevelOneEnd : MonoBehaviour
{
    public Camera cam1;
    public Camera cam2;


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("Switched Camera");
            cam1.enabled = false;
            cam2.enabled = true;
            cam2.transform.DOMove(new Vector3(-17f, -5f, 776.48f), 8f);
        }
    }

}
