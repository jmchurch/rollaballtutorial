using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraController : MonoBehaviour
{
    public GameObject player;
    public ParticleSystem speedLines;

    private Vector3 offset;
    private Vector3 newOffset;
    private float playerSpeed;


    private void Awake()
    {
        speedLines.Pause();
    }

    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - player.transform.position;
        newOffset = offset;
        Debug.Log("Offset Magnitude: " + offset.magnitude);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = player.transform.position + newOffset;
    }

    private void Update()
    {
        playerSpeed = player.GetComponent<Rigidbody>().velocity.magnitude;
        if (playerSpeed > 20)
        {
            if (speedLines.isPaused) speedLines.Play(); // If the particle system is currently paused it will be played
            speedLines.startSpeed = playerSpeed; // currently is bad ig, will change after I see if this works
            // update the speed of the lines with the player speed, this seems to be a pretty good ratio that looks good
        }
        else
        {
            if (speedLines.isPlaying)
            {
                speedLines.Pause(); // If the speed is less than 20 and the particle system is playing it will be paused
                speedLines.Clear(); // It will then be cleared
            }
        }
    }

    public void rightTurn()
    {
        // I did the work in my notes to figure this out, but this is just how the offset changes as you move in 90 degree sections around the player
        transform.DOMove(new Vector3(offset.z, offset.y, offset.x * -1), 1.5f);
        newOffset = new Vector3(offset.z, offset.y, offset.x * -1);
        transform.DORotate(new Vector3 (10, 90, 0), .5f);
    }

    public void leftTurn()
    {
        transform.DOMove(new Vector3(offset.z * -1, offset.y, offset.x), 1.5f);
        newOffset = new Vector3(offset.z *-1, offset.y, offset.x);
        transform.DORotate(new Vector3(10, -90, 0), .5f);
    }

    public void backwards()
    {
        transform.DOMove(new Vector3(offset.x, offset.y, offset.z * -1), 1.5f);
        newOffset = new Vector3(offset.x, offset.y, offset.z * -1);
        transform.rotation = Quaternion.Euler(10, 180, 0);
    }

    public void forwards()
    {
        transform.DOMove(offset, 1.5f);
        newOffset = offset;
        transform.DORotate(new Vector3(10, 0, 0), .5f);
    }


    // Idea for rotating the camera
    /*
     * In the Vector3 the Y value will stay the same, but the X value and the Z value will need to change
     * (They will also need to change over time so that it isn't jarring)
     * Possible (loser solution) Solution, have four cameras that are pointing at the player from all directions that can be switched to
     * 
     * Possible Solution: Do math
     * Off start
     * Camera Pos: new Vector3 (-1.1, 3.2, -14.8)
     * Player Pos: new Vector3 (-0.3, 0.4, -7.0)
     */
}
