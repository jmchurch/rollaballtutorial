using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public float distToGround;
    Collider rb_Collider;
    public AudioSource pickUpSound;
    public GameObject pickUpParticles;
    public float speed = 0;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;

    private Rigidbody rb;
    private float movementX;
    private float movementY;
    private int count;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        rb_Collider = rb.GetComponent<Collider>();
        distToGround = rb_Collider.bounds.extents.y + (float) 0.1;

        SetCountText();
        winTextObject.SetActive(false);
    }

    private void Update()
    {
        // when jump button is pressed add an upward force to the player
        if (Input.GetButtonDown("Jump") && isGrounded())
        {
            rb.AddForce(new Vector3(0f, 200f, 0f));
        }
    }

    private bool isGrounded()
    {
        return Physics.Raycast(transform.position, -Vector3.up, distToGround);
    }

    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 19) winTextObject.SetActive(true);
    }

    void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);
        rb.AddForce(movement * speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp")) other.gameObject.SetActive(false);
        count++;
        SetCountText();

        // play PickUpSound
        pickUpSound.Play();

        // play pickUpParticles
        GameObject particles = Instantiate(pickUpParticles, this.transform.position, Quaternion.identity);
      
        // after 10 seconds destroy the GameObject particles
        Destroy(particles, 6f);
    }
}
